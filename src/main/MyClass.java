package main;

import java.util.Scanner;

public class MyClass {

	public static void main(String[] args) {
		
		int x = 5;
		byte z = (byte) x;
		System.out.println(z);
		
		System.out.println("Hello world from console");
		
		String str = new String("bonjour");
		System.out.println(str);
		
		Integer nbr = new Integer(10);
		System.out.println(nbr);
		
		String mystr = "Bonjour les bons jours";
		String str2 = mystr.replace("jour","soir");
		System.out.println(str2);
		
		//convert integer to string
		int zy = 4;
		String str3 = Integer.toString(zy);
		System.out.println(str3);
		
		int u = 6;
		String s = "" + u;
		System.out.println(s);
		
		//Concatenate two strings
		
		String un = "Salut";
		String deux = "tout le monde";
		String trois = un + " " + deux;
		System.out.println(trois);
		
		StringBuilder sb = new StringBuilder();
		sb.append(un).append(" ").append(deux);
		String quatre = sb.toString();
		System.out.println(quatre);
		
		
		System.out.println(quatre.getClass().getSimpleName());
		
		//post increment
		
		int i = 2;
		int j = i++;
		System.out.println(i);
		System.out.println(j);
		//pre increment
		int k = 2;
		int l = ++k;
		System.out.println(k);
		System.out.println(l);
		
		//instance Scanner class
//		System.out.println("veuillez rentrer deux nombre:");
//		Scanner scan = new Scanner(System.in);
//		int m =  scan.nextInt();
//		System.out.println(m);
//		scan.close();
		
		//next will just display one word, nextLine displays the whole sentence
//		System.out.println("veuillez rentrer une phrase:");
//		Scanner scan = new Scanner(System.in);
//		String m =  scan.nextLine();		
//		System.out.println(m);		
//		scan.close();
		
		//with next
//		System.out.println("veuillez rentrer une phrase:");
//		Scanner scan1 = new Scanner(System.in);
//		String mot1 =  scan1.next();
//		String mot2 =  scan1.next();
//		System.out.println(mot1);	
//		System.out.println(mot2);
//		scan1.close();
		
	
		System.out.println("veuillez rentrer une phrase:");
		Scanner scan1 = new Scanner(System.in);
		String mot1 =  scan1.next();
		String mot2 =  scan1.next();
		String total = mot1 +" "+ mot2;
		System.out.println(total);
		scan1.close();		

		
		
	}

}
