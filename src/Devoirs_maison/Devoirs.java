package Devoirs_maison;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.Spring;
import javax.tools.JavaCompiler;

public class Devoirs {

	public static void main(String[] args) {
	
		//Copier un array dans un nouveau array
//		int [] monArray = {2,4,6,8};
//		int [] deuxiemeArray = {1,3,5,7};
//		
//		int [] newArr = new int[8];
//		System.arraycopy(monArray, 0, newArr, 0, 4);
//		System.arraycopy(deuxiemeArray, 0, newArr, 4, 4);
//		
//		for (int n : newArr) {
//            System.out.print(n + " ");
//        }
//        System.out.println();
        
        //�crivez un programme Java pour trouver les valeurs en double d'un tableau de valeurs enti�res
        
//        int [] monArray = {2,4,6,8,1,14,8,6,4};
//        Arrays.sort(monArray);
//        
//        for(int i = 0; i < monArray.length-1; i++) {
//        	
//        		if( monArray[i] == monArray[i+1]) {
//        			System.out.print(monArray[i] + " " + "a un doublon dans le tableau"); 
//        			System.out.println();
//        		}	    		
//        	}   
//        	
//        }
//        
        //�crivez un programme Java pour remplacer une cha�ne "python" par "java" et "java" par "python" dans une cha�ne donn�e.

//        String str = "Python is more popular than Java"; 
//        String[] arrOfStr = str.split(" ");
//        ArrayList<String> newArr = new ArrayList<String>();
//        
//        for (int i = 0; i < arrOfStr.length; i++) { 
//        	String value = arrOfStr[i];        	
//        	newArr.add(value);        	
//         }
//        
//        for(int j = 0; j < newArr.size(); j++) { 	
//        		
//    		String newVal = newArr.get(j);
//    	   		
//            if(newVal.equals("Python")) {
//            	
//            	newArr.set(j, "java");
//            	
//            } else if (newVal.equals("Java")) {
//            	
//            	newArr.set(j, "python");
//            	
//            }            	
//    	}
//        
//       
//        	String joinedStr = String.join("  ", newArr);
//        	System.out.print(joinedStr);
		
//�crivez un programme Java qui accepte quatre entiers de l'utilisateur et imprime �gal si tous les quatre sont �gaux, et non �gaux sinon. 	
		
//		Scanner scan = new Scanner(System.in);
//		System.out.println("Entrez 4 nombres entiers : ");
//		int j = 0;
//		int nbr;
//				
//		int [] arrNbr = new int[4];
//		
//		for(int i = 1; i <= arrNbr.length ; i++) {
//			System.out.println("nombre " + i);
//			nbr = scan.nextInt();
//			arrNbr[j] = nbr;
//			j++;	
//		}
//		if(arrNbr[0] == arrNbr[1] && arrNbr[2] == arrNbr[3]) {
//			System.out.print("�gal!");
//		} else {
//			System.out.print("pas �gal!");
//		}
//		scan.close();
		
		//�crivez un programme Java pour afficher la vitesse, en m�tres par seconde, kilom�tres par heure et miles par heure.Saisie utilisateur: distance (en m�tres) et le temps a �t� pris (sous forme de trois chiffres: heures, minutes, secondes)
    
//		Scanner scan = new Scanner(System.in);
//		System.out.println("Entrez une distance en m�tres: ");
//		int distance = scan.nextInt();
//		System.out.println("Entrez le temps mis pour parcourir cette distance en heure: ");
//		int heure = scan.nextInt();
//		System.out.println("Entrez le temps mis pour parcourir cette distance en minutes: ");
//		int minutes = scan.nextInt();
//		System.out.println("Entrez le temps mis pour parcourir cette distance en secondes: ");
//		int secondes = scan.nextInt();
//		
//		int [] arrTemps = {heure,minutes,secondes};		
//		for(int i = 0; i <arrTemps.length; i++) {
//			System.out.print(arrTemps[i]+ " ");
//		}
//		
//		int hourToSec = arrTemps[0]* 3600;
//		int minToSec = arrTemps[1] * 60;
//		double ratio = 3.6;
//		double ratioMiles = 5793.64;
//		double mToMiles = 0.000621371;
//		double distanceMiles = distance * mToMiles;
//		float totalSeconde = (distance/(hourToSec + minToSec + arrTemps[2]));
//		double totalKmHeure = (distance/(hourToSec + minToSec + arrTemps[2]))* ratio;
//		double milesPerHour = (distanceMiles/(hourToSec + minToSec + arrTemps[2]))* ratioMiles;
//		System.out.println("La vitesse en m�tres par secondes est de: " + totalSeconde);
//		System.out.println("La vitesse en km/h est de: " + totalKmHeure);
//		System.out.println("La vitesse en miles/h est de: " + milesPerHour);
//		scan.close();
		
	//�crivez un programme Java qui accepte trois nombres et affiche "Tous les nombres sont �gaux" si les trois nombres sont �gaux, "Tous les nombres sont diff�rents" si les trois nombres sont diff�rents et sinon "Ni tous ni �gaux ni diff�rents".
//		Scanner scan = new Scanner(System.in);
//		System.out.println("Entrez 3 nombres entiers : ");
//		int j = 0;
//		int nbr;
//				
//		int [] arrNbr = new int[3];
//		
//		for(int i = 1; i <= arrNbr.length ; i++) {
//			System.out.println("nombre " + i);
//			nbr = scan.nextInt();
//			arrNbr[j] = nbr;
//			j++;	
//		}
//		if(arrNbr[0] == arrNbr[1] && arrNbr[1] == arrNbr[2] ) {
//			System.out.print("Tous les nombres sont �gaux");
//		} else if(arrNbr[0] == arrNbr[1] && arrNbr[1] != arrNbr[2] || arrNbr[0] == arrNbr[2] && arrNbr[1] != arrNbr[2] || arrNbr[0] != arrNbr[2] && arrNbr[1] == arrNbr[2]) {
//			System.out.print("Ni tous �gaux ni diff�rents!");
//		} else {
//			System.out.print("Tous diff�rents!");
//		}
//		scan.close();
		
		//�crivez un programme Java pour trouver les plus petits et les deuxi�mes plus petits �l�ments d'un tableau donn�
		
//		int [] tab = {5, 7, -8, 5, 14, 1};
//		
//		Arrays.sort(tab);
//		System.out.println("le plus petit �l�ment est:" + " " + tab[0]);
//		System.out.println("le deuxi�me plus petit �l�ment est:" + " " + tab[1]);
		
		//Ecrire un programme pour que pour un cours du dollar donn� (et qui ne change pas pendantl�ex�cution du programme), on puisse convertir en euro. 
		//Pr�voyez un moyen d�arr�ter l�ex�cution du programme.
		
		
//		System.out.println("    " + "Conversion Dollar us vs Euro");		
//		double dollarVal;
//		double euroVal;
//		int result;
//		Scanner scan = new Scanner(System.in);
//		System.out.println("Donner le cours du Dollar us vs Euro(5 chiffres apr�s la virgule)");
//		euroVal = scan.nextDouble();
//		
//		do {
//			System.out.println("1. Convertir en euros");
//			System.out.println("2. Sortir");
//			result = scan.nextInt();
//			
//			if(result == 1) {
//			System.out.println("Entrez le montant en dollar us:");
//			dollarVal = scan.nextDouble();
//			double convert = dollarVal * euroVal;
//			System.out.println("la valeur en euros est de : " + convert + " euros");
//			}
//		} while (result != 2);
//		
//		System.out.print("Au revoir!");
//		scan.close();
	} 
}      
		

